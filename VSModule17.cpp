﻿#include <iostream>
#include <cmath>
class ExampleClass
{
public:

	void Print()
	{
		std::cout << message << std::endl;
	}

	int PrintNumber()
	{
		return ModuleNumber;
	}

private:
	std::string message = "Почему классы на плюсах гораздо понятнее и проще чем на шарпе...";
	int ModuleNumber = 34;
};

class Vector
{
public:

	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	double VectorLength()
	{
		return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}

private:

	double x;
	double y;
	double z;
};


int main()
{
	setlocale(0, "Russian");
	Vector vec(10, 20, 10);
	
	ExampleClass vers1;
	
	vers1.Print();

	std::cout << "Ещё каких то " << vers1.PrintNumber() << " модуля и я джун, уряя!" << std::endl;
	
	std::cout <<"Длина вектора составляет: " << vec.VectorLength() << std::endl;
}